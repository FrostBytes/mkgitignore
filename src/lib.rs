extern crate toml;
extern crate reqwest;

pub mod gitignore;
pub mod fetch;

fn str_cmp(first: &str, last: &str) -> i32 {
    // create a diff
    let mut diff = 0;

    // zip up and loop through string
    let mut firsti = first.chars();
    let mut lasti = last.chars();
    loop {
        // try to get a character
        let firstc = firsti.next();
        let lastc = lasti.next();

        if firstc.is_some() ^ lastc.is_some() {
            // one iterator has ended too soon!
            diff += 1;
        } else if firstc.is_some() && firstc.is_some() {
            // both iterators are supplying!
            let firstc = firstc.unwrap();
            let lastc = lastc.unwrap();

            // if both characters are not equal, then we have a conflict.
            if firstc != lastc {
                diff += 1;
            }
        } else {
            // no iterators are supplying!
            break diff;
        }
    }
}