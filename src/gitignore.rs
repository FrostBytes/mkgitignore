use std::io::{Write, Read as _, Error as IoError};
use std::fs::File;

use crate::fetch::{Fetcher, HttpFetcher, StringFetcher};

use toml::Value;
use toml::de::Error as TomlError;

// PROVIDER
pub struct Provider {
    name: String,
    alias: Vec<String>,
    fetch: Box<dyn Fetcher>,
}

impl Provider {
    pub fn write_to<T: Write>(&self, stream: &mut T) -> Result<(), Box<dyn std::error::Error>> {
        self.fetch.write_to(stream)
    }

    pub fn name(&self) -> &str { &self.name }
    pub fn aliases(&self) -> std::slice::Iter<'_, String> { self.alias.iter() }
}

pub struct ProviderParseError {
    toml: Option<TomlError>,
    io: Option<IoError>,
    message: String,
}

impl ProviderParseError {
    fn from_str(s: &str) -> ProviderParseError {
        <Self as From<String>>::from(String::from(s))
    }

    fn from_toml(e: TomlError, file: &str) -> ProviderParseError {
        ProviderParseError {
            message: format!("toml ({}): {}", file, e.to_string()),
            toml: Some(e),
            io: None,
        }
    }
}

impl std::fmt::Display for ProviderParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        f.write_str(&self.message)
    }
}

impl std::fmt::Debug for ProviderParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        <Self as std::fmt::Display>::fmt(self, f)
    }
}

impl std::error::Error for ProviderParseError {
    fn cause(&self) -> Option<&dyn std::error::Error> {
        // match io error
        match &self.io {
            Some(io) => return Some(io),
            None => (),
        };

        // match toml error if present
        match &self.toml {
            Some(toml) => Some(toml),
            None => None,
        }
    }
}

impl From<IoError> for ProviderParseError {
    fn from(e: IoError) -> ProviderParseError {
        ProviderParseError {
            message: format!("io: {}", e.to_string()),
            io: Some(e),
            toml: None,
        }
    }
}

impl From<String> for ProviderParseError {
    fn from(s: String) -> ProviderParseError {
        ProviderParseError {
            message: s,
            toml: None,
            io: None,
        }
    }
}

pub struct MatchPair<'a> {
    pub(self) matched: String,
    pub(self) provider: &'a Provider,
}

impl<'a> MatchPair<'a> {
    pub fn write_to<T: Write>(&self, stream: &mut T) -> Result<(), Box<dyn std::error::Error>> {
        self.provider.write_to(stream)
    }

    pub fn matched(&self) -> &str {
        &self.matched
    }
}

pub struct SearchResult<'a> {
    pub(self) exact_match: Option<MatchPair<'a>>,
    pub(self) close_matches: Vec<MatchPair<'a>>,
}

impl<'a> SearchResult<'a> {
    pub fn exact(&self) -> &Option<MatchPair<'a>> {
        &self.exact_match
    }

    pub fn close(&self) -> std::slice::Iter<'_, MatchPair<'a>> {
        self.close_matches.iter()
    }
}

pub struct ProviderCollection {
    providers: Vec<Provider>
}

impl ProviderCollection {
    pub fn new() -> ProviderCollection {
        ProviderCollection {
            providers: Vec::new(),
        }
    }

    pub fn parse<S: AsRef<std::path::Path>, I: Iterator<Item = S>>(configs: I) -> Result<ProviderCollection, ProviderParseError> {
        // create a common provider collection
        let mut coll = ProviderCollection::new();

        // create a buffer to store config contents
        let mut buf = String::new();

        for config in configs {
            // open file
            let mut file = File::open(&config)?;
            file.read_to_string(&mut buf)?;

            // parse file
            ProviderCollection::parse_single(&mut coll, &config, &buf)?;

            // clear string
            buf.clear();
        }

        Ok(coll)
    }
    
    pub fn parse_single<S: AsRef<std::path::Path>>(prov: &mut ProviderCollection, file: S, config: &str) -> Result<(), ProviderParseError> {
        let conf = match config.parse::<Value>() {
            Ok(parse) => parse,
            Err(err) => return Err(ProviderParseError::from_toml(err, file.as_ref().file_name().unwrap().to_str().unwrap()))
        };

        // get gitignores
        let gitignores = match conf.get("gitignore") {
            Some(arr) => match arr {
                Value::Array(a) => a.clone(),
                _ => return Err(ProviderParseError::from_str("\"gitignore\" expected as array")),
            },
            None => Vec::<Value>::new()
        };

        // loop through gitignores
        for gitignore in gitignores {
            match gitignore {
                Value::Table(t) => {
                    // create a provider
                    let provider = Provider {
                        name: match t.get("name") {
                            Some(s) => match s.as_str() {
                                Some(s) => String::from(s),
                                None => return Err(ProviderParseError::from_str("expected a string for \"name\"")),
                            },
                            None => return Err(ProviderParseError::from_str("expected \"name,\" but was not found")),
                        },
                        alias: match t.get("alias") {
                            Some(a) => match a.as_array() {
                                Some(a) => a.iter().map(|v| {
                                    if let Value::String(s) = v {
                                        s.clone()
                                    } else {
                                        String::new()
                                    }
                                }).filter(|s| !s.is_empty()).collect(),
                                None => return Err(ProviderParseError::from_str("expected an array for \"alias\"")),
                            },
                            None => Vec::<String>::new(),
                        },
                        fetch: {
                            if let Some(t) = t.get("url") { // check if we can retrieve this file from a url (http, file, ftp...)
                                match t {
                                    Value::String(url) => {
                                        // we can get this from a url!
                                        // check if this is a http method
                                        if url.starts_with("http://") || url.starts_with("https://") {
                                            Box::new(HttpFetcher::new(url))
                                        } else {
                                            // return an error
                                            return Err(ProviderParseError::from_str("no suitable method found for \"url\""))
                                        }
                                    },
                                    _ => return Err(ProviderParseError::from_str("expected a string for \"url\""))
                                }
                            } else if let Some(t) = t.get("text") { // check if we can get this by text
                                match t {
                                    Value::String(text) => {
                                        // read the text
                                        Box::new(StringFetcher::new(text))
                                    },
                                    _ => return Err(ProviderParseError::from_str("expected a string for \"text\""))
                                }
                            } else {
                                // if no possible fetchers are available, then return an error
                                return Err(ProviderParseError::from_str("no suitable fetcher found!"))
                            }
                        }
                    };

                    // add it to the list
                    let name_provider = String::from(provider.name());
                    match prov.add(provider) {
                        Err(_) => return Err(format!("provider \"{}\" is defined twice!", name_provider).into()),
                        _ => (),
                    };
                },
                _ => return Err(ProviderParseError::from_str("expected table as gitignore")),
            }
        }

        Ok(())
    }

    pub fn add(&mut self, prov: Provider) -> Result<(), ()> {
        // check if we can find a provider with the same name
        if self.providers.iter().any(|p| p.name() == prov.name()) {
            // there is already something that exists!
            Err(())
        } else {
            // add the provider
            self.providers.push(prov);

            Ok(())
        }
    }

    pub fn search(&self, search: &str) -> SearchResult {
        // force the search to be lowercase
        let search = search.to_lowercase();

        // create a searchresult
        let mut res = SearchResult {
            exact_match: None,
            close_matches: Vec::new(),
        };

        // loop through providers we have
        'top: for provider in self.providers.iter() {
            // compare to name first
            let name_match = crate::str_cmp(&provider.name().to_lowercase(), &search);
            if name_match <= 0 {
                // this is an exact match!
                res.exact_match = Some(MatchPair {
                    matched: String::from(provider.name()),
                    provider,
                });
                break;
            }

            // try a close match
            if name_match < 4 {
                // close match!
                res.close_matches.push(MatchPair {
                    matched: String::from(provider.name()),
                    provider,
                });
            }

            // check aliases next
            for alias in provider.aliases() {
                let alias_match = crate::str_cmp(&alias.to_lowercase(), &search);
                if alias_match == 0 {
                    // this is an exact match!
                    res.exact_match = Some(MatchPair {
                        matched: String::from(alias),
                        provider,
                    });
                    break 'top;
                }

                // try a close match
                if alias_match < 4 {
                    // close match!
                    res.close_matches.push(MatchPair {
                        matched: String::from(alias),
                        provider,
                    })
                }
            }
        }

        // return search result
        res
    }
}