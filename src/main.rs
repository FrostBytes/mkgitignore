extern crate clap;

use clap::{Arg, App};

use std::fs::File;
use std::path::Path;
use std::process::exit;
use std::io::{self, Write as _};

use reqwest::blocking;

use mkgitignore::gitignore::ProviderCollection;

// CONFIG
pub const STARTER_CONFIGS: [&'static str; 2] = [
    "https://gitlab.com/FrostBytes/mkgitignore/-/raw/master/config/github.toml",
    "https://gitlab.com/FrostBytes/mkgitignore/-/raw/master/config/ide.toml",
];

pub const CONFIG_PLACES: [&'static str; 1] = [
    "~/.config/mkgi",
];

pub fn treat_path(s: &str) -> std::path::PathBuf {
    // replace any ~ with home
    let home = match std::env::var("HOME") {
        Ok(s) => s,
        Err(_) => panic!("home is not set!"),
    };

    let repl = s.replace("~", &home);

    std::path::PathBuf::from(repl)
}

/// looks for configs (.toml) files in obvious places such as:
///   * `/etc/mkgi`
///   * `~/.config/mkgi`
pub fn find_configs() -> Vec<std::path::PathBuf> {
    let mut places = Vec::<std::path::PathBuf>::new();

    for path in CONFIG_PLACES.iter().map(|s| treat_path(s)) {
        // check if the path exists
        if path.exists() {
            // check if the path is a dir
            if path.is_dir() {
                // add dir
                walk_config_dir(path, &mut places);
            } else {
                // make an error message!
                println!("warning! config path {} is a file!", path.to_str().unwrap());
            }
        } else {
            // if it doesn't, then try to fill it up with files
            // create the dir first
            match std::fs::create_dir_all(&path) {
                Ok(_) => (),
                Err(_) => continue, // if it can't create the dir, then permissions are lacking! skip...
            }

            // download configs
            match download_configs(&path) {
                Ok(_) => (),
                Err(err) => println!("warning! failed to download configs!\n{} @ {}", err, path.display()),
            }

            // add dir
            walk_config_dir(path, &mut places);
        }
    };

    places
}

pub fn walk_config_dir<S: AsRef<std::path::Path>>(path: S, vec: &mut Vec<std::path::PathBuf>) {
    let paths = std::fs::read_dir(path).unwrap();

    for path in paths {
        let path = match path {
            Ok(path) => path,
            Err(_) => continue, // skip over errored files
        }.path();
        let filename = path.file_name().unwrap().to_str().unwrap();

        if !path.is_dir() {
            // if this is a toml file, then add it to the config files
            if filename.ends_with(".toml") {
                vec.push(path.to_path_buf());
            }
        }
    }
}

/// download a set of starter configs from gitlab.com
pub fn download_configs<S: AsRef<std::path::Path>>(path: S) -> Result<(), Box<dyn std::error::Error>> {
    let path = path.as_ref();

    for starter in STARTER_CONFIGS.iter() {
        // get file name
        let filename = starter.split('/').last().unwrap();

        // create a buffer
        let mut buf = path.to_path_buf();
        buf.push(filename);

        // download files
        let mut res = blocking::get(*starter)?;
        if res.status().is_success() {
            // open file and write contents to it
            let mut file = std::fs::File::create(&buf)?;

            res.copy_to(&mut file)?;
        } else {
            println!("link {} is returning an error ({}). if this persists, file a bug!", starter, res.status());
        }
    }

    Ok(())
}

// ARGUMENTS
struct Arguments {
    specs: Vec<String>,
    appendage: String,
    output: String,
    force: bool,
}

fn get_args() -> Arguments {
    let matches = App::new("mkgitignore")
        .version("0.1.0")
        .author("FrostBytes <cprootintootinsecrets@gmail.com>")
        .about("Easily create a .gitignore file from external sources of your choosing.")
        .arg(Arg::with_name("force")
            .help("Replace files instead of warning.")
            .short("f")
            .long("force"))
        .arg(Arg::with_name("append")
            .help("Append text to the end of the .gitignore")
            .short("A")
            .long("append")
            .takes_value(true)
            .value_name("APPENDAGE"))
        .arg(Arg::with_name("output")
            .help("Redirect output to a different file")
            .short("O")
            .long("output")
            .default_value(".gitignore")
            .value_name("OUTPUT"))
        .arg(Arg::with_name("SPEC")
            .help("Spec or specs to fetch.")
            .required(true)
            .multiple(true)
            .index(1))
        .get_matches();
    
    Arguments {
        specs: matches.values_of("SPEC").unwrap().map(|s| String::from(s)).collect(),
        appendage: match matches.value_of("append") {
            Some(s) => String::from(s),
            None => String::new(),
        },
        output: String::from(matches.value_of("output").unwrap()),
        force: matches.occurrences_of("force") > 0,
    }
}

// APP CODE
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = get_args();

    // open file to output
    let mut ofile = if Path::new(&args.output).is_file() {
        if args.force {
            // clear the file
            std::fs::OpenOptions::new().write(true).truncate(true).open(&args.output)?
        } else {
            // warn the user about overwriting files
            println!("warning! file {} already exists in this directory!", args.output);
            println!("to overwrite the file, pass the -f / --force argument.");
            exit(1);
        }
    } else {
        File::create(&args.output)?
    };

    // try to parse files
    let coll = match ProviderCollection::parse(find_configs().iter()) {
        Ok(coll) => coll,
        Err(err) => {
            // print error and exit
            write!(io::stderr(), "failed to parse config!\n{}\n", err.to_string())?;
            exit(1);
        },
    };

    // loop through specs and download them
    for spec in args.specs.iter() {
        println!("searching for spec {}...", spec);

        let res = coll.search(spec);
        if let Some(m) = res.exact() {
            // we found a match!
            println!("match found! fetching {}...", m.matched());

            // write
            m.write_to(&mut ofile)?;

            // also write a newline character
            ofile.write(b"\n")?;
        } else {
            // check for any close matches
            for (i, close) in res.close().enumerate() {
                if i == 0 {
                    // print a suggestion message
                    println!("{} was not able to be found, but maybe you meant...", spec)
                }

                // print information
                println!("\t=> {}", close.matched())
            }

            println!("spec {} not found!", spec);

            exit(1);
        }
    }

    // add appendage
    ofile.write(args.appendage.as_bytes())?;

    Ok(())
}
