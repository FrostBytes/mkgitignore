use std::io::Write;
use std::error::Error;

// FETCHERS
pub trait Fetcher {
    fn write_to(&self, stream: &mut dyn Write) -> Result<(), Box<dyn Error>>;
}

// HTTP FETCHERS
pub struct HttpFetcher {
    url: String,
}

impl HttpFetcher {
    pub fn new(url: &str) -> HttpFetcher {
        HttpFetcher {
            url: String::from(url),
        }
    }
}

impl Fetcher for HttpFetcher {
    fn write_to(&self, stream: &mut dyn Write) -> Result<(), Box<dyn Error>> {
        let mut res = reqwest::blocking::get(&self.url)?; // syncronously wait for get

        // check if there was a success
        if res.status().is_success() {
            res.copy_to(stream)?;

            Ok(())
        } else {
            panic!("HOW.")
        }
    }
}

// TEXT FETCHERS
pub struct StringFetcher {
    string: String,
}

impl StringFetcher {
    pub fn new(string: &str) -> StringFetcher {
        StringFetcher {
            string: String::from(string),
        }
    }
}

impl Fetcher for StringFetcher {
    fn write_to(&self, stream: &mut dyn Write) -> Result<(), Box<dyn Error>> {
        // write string to stream
        stream.write(self.string.as_bytes())?;

        Ok(())
    }
}