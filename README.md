# mkgitignore
`mkgitignore` is a simple small Rust program that allows very, VERY quick instantiation of a `.gitignore` file in your current working directory. Good if you're a trigger happy programmer like me. It has the ability to download config files (which are key to making `mkgitignore` work) from the internet automatically.

### DISCLAIMER
Currently, this software uses linux paths and tries to read linux-based environment variables. If you would like to test this software on your Windows machine while 0.3.0 is coming out, you may set the `HOME` environment variable to your user path.

## Installation
Installation is simple. To install the [binary](https://crates.io/crates/mkgitignore), you must have `rustc` and `cargo` installed on your system. You may either download the binary from crates.io with...
```
cargo install --version 0.2.0 mkgitignore
```

Or you may install mkgitignore directly from the git repository with...
```
cargo install --git https://gitlab.com/FrostBytes/mkgitignore --tag 0.2.0

for bleeding edge:
cargo install --git https://gitlab.com/FrostBytes/mkgitignore
```

## Configuration
`mkgitignore` works by fetching up a list of gitignores, their aliases and how to get them (via. http or text). The configuration is in TOML. Each entry is stored in an array of tables named `gitignore`. You may use the clutterly inline table option to create config files, but I think that the built in "*array of table*" syntax is easier on the eyes.
```toml
[[gitignore]]
name = "C++"
url = "https://raw.githubusercontent.com/github/gitignore/master/C++.gitignore"
alias = ["cpp", "cplusplus"]
```

```toml
[[gitignore]]
name = "system files"
text = """
# ignore system files
*.sys"""
alias = ["sysfiles"]
```
When using a spec, `mkgitignore` will first match against the name of the gitignore against the spec passed to the program, and then match the aliases. You cannot exact match more than one provider (`mkgitignore` will catch this and error!), but you may exact match aliases. The last gitignore read (in order lexicographically by filename) will always has dominance over the other gitignores. This may be patched or kept in as a feature.

* On Linux/UNIX (also MacOS), config files are stored in `~/.config/mkgi`.
* ~~On Windows, config files are stored in `%LOCALAPPDATA%\mkgi`.~~

There can be multiple config files in the same config directory. `mkgitignore` **will** not run if it finds two gitignores with the same name defined in the same directory. Config files should end with `.toml`, or they will be ignored.

### By Url
Adding a `url` string to your gitignore enables a whole variety of things. Currently, it only supports `http://` and `https://`, but this will be extended further in the future to support `file://` and `ftp://` (`sftp://` too).

### By Text
You may simply *inline* your gitignore within a config file if you're running 0.2.0 or you don't want to create a static gitignore file on your disk. For this, you should add the `text` string to your config.